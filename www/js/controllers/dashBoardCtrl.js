/**
 * Created by atul on 3/9/2016.
 */
angular.module('kyc')
.controller('DashBoardCtrl', function($state,$scope,$rootScope,$cordovaSQLite,kycServiceSaveData,$http,$q ,$ionicLoading,$ionicPopup,SavephotoService,kycServiceGetData) {
        var results = [];
        var total_upload = 0;
        var userid ;
        userid = $rootScope.user_id;
        $scope.checkLocalData = function () {
            var query = "SELECT rowid, B.*, ";
            query += "(SELECT group_concat(brand_id,',') ";
            query += "FROM tbl_kyc_brand A ";
            query += "where mapping = 1 AND A.kyc_id = B.rowid GROUP BY A.kyc_id) as brnds ";
            query += "FROM tbl_kyc B where user_id="+ $rootScope.user_id ;
            query +=  " order by B.id desc";
            $cordovaSQLite.execute(db, query).then(function (res) {
                if (res.rows.length > 0) {
                    for (var i = 0; i < res.rows.length; i++) {
                        var obj = {};
                        obj = {
                            user_id : res.rows.item(i).user_id,
                            uploaded: res.rows.item(i).uploaded,
                            device_rowid: res.rows.item(i).rowid,
                            form_no: res.rows.item(i).form_no,
                            owner_name: res.rows.item(i).owner_name,
                            bussiness_name: res.rows.item(i).bussiness_name,
                            building_name: res.rows.item(i).building_name,
                            street_name: res.rows.item(i).street_name,
                            landmark: res.rows.item(i).landmark,
                            state: res.rows.item(i).state,
                            district: res.rows.item(i).district,
                            city: res.rows.item(i).city,
                            area: res.rows.item(i).area,
                            pincode: res.rows.item(i).pincode,
                            mobile_1: res.rows.item(i).mobile_1,
                            mobile_2: res.rows.item(i).mobile_2,
                            landline: res.rows.item(i).landline,
                            email: res.rows.item(i).email,
                            website: res.rows.item(i).website,
                            shop_size: res.rows.item(i).shop_size,
                            dob: res.rows.item(i).dob,
                            image_path: res.rows.item(i).local_img_path,
                            image_path1: res.rows.item(i).local_img_path1,
                            image_path2: res.rows.item(i).local_img_path2,
                            brandlist: res.rows.item(i).brnds,
                            industry: res.rows.item(i).industry,
                            smartphone: res.rows.item(i).smartphone,
                            competitive_brands: res.rows.item(i).competition
                        }

                        if (obj.uploaded == 0) {
                            results.push(obj);

                        }

                    }

                    $scope.kyc_lists = results;

                    $scope.$root.kyc_lists_count = results.length;


                }

                else {

                    $scope.$root.kyc_lists_count = 0;

                }


            }, function (err) {
                console.error(JSON.stringify(err));
            });

        }

        init();
        function init() {
            $scope.checkLocalData();

        }

        $scope.doRefresh = function()
        {
            $scope.SelectKycByUserId(userid);
            $scope.SelectUserAllocation(userid);
            // Stop the ion-refresher from spinning
            $scope.$broadcast('scroll.refreshComplete');
        }
        $scope.SelectKycByUserId = function (userid)
        {
            var approved_count=0;
            var pending_approval_count= 0;
            var rejected_count = 0;

            kycServiceGetData.SelectKycByUserId($http, $q, userid).then(function (data) {
                $scope.KycByUserId=data;
                for(i=0;i<data.length;i++)
                {
                    var query = "UPDATE tbl_kyc SET approval_status = "+ data[i].approval_status + ",reject_remarks = " + "'"+ data[i].reject_remarks + "'"+ ",reject_reason=" + "'"+ data[i].reject_reason + "'"+ " where db_key ="+ data[i].kyc_id+"";

                    $cordovaSQLite.execute(db, query);

                    if (data[i].approval_status ==0){
                        pending_approval_count = pending_approval_count+1;

                    }
                    if (data[i].approval_status ==1){
                        approved_count = approved_count+1;

                    }

                    if (data[i].approval_status ==2){
                        rejected_count = rejected_count+1;


                    }
                }
                $rootScope.pending_approval_count=pending_approval_count;
                $rootScope.approved_count=approved_count;
                $rootScope.rejected_count=rejected_count;
            })

        }

        $scope.SelectUserAllocation = function (userid) {

            kycServiceGetData.SelectUserAllocation($http, $q, userid).then(function (data) {

                var qry=  "DELETE  from user_allocation ";
                $cordovaSQLite.execute(db,qry);
                for(i=0;i<data.length;i++)
                {
                    var userAreaID = data[i].userAreaID;
                    var areaid =   data[i].areaid;
                    var state = data[i].state;
                    var district =data[i].district;
                    var city = data[i].city;
                    var area = data[i].area;
                    var pincode = data[i].pincode;

                    var query=  "INSERT INTO user_allocation (userAreaID,areaid,state,district,city,area,pincode)  VALUES (?,?,?,?,?,?,?) ";
                    $cordovaSQLite.execute(db, query,[userAreaID,areaid,state,district,city,area,pincode]).then(function(res) {
                    }, function (err) {
                        console.error(JSON.stringify(err));
                    });


                }

            })

        }


    })