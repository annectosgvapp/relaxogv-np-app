/**
 * Created by atul on 3/25/2016.
 */
angular.module('kyc')
.controller('LoginCtrl', function($http, $q,$scope, $state, $rootScope,$ionicPopup, AuthService,$ionicLoading,kycServiceGetData,$cordovaSQLite) {


    $scope.data = {};
    var userid;
    var results =[];


    $scope.login = function(data) {
        $rootScope.user_name=data.username;
        $ionicLoading.show({
            template: 'Logging In Please Wait.....'
        });
        if($rootScope.isOnline==true){
        AuthService.login($http, $q,data.username, data.password).then(function(authenticated) {
            $ionicLoading.hide();
            var return_data = authenticated.toString();
            var indx1 =return_data.indexOf(",") + 1;
            var indx2 =return_data.indexOf(",",indx1) + 1;
            $rootScope.user_id = return_data.substr(indx1, (indx2-indx1-1));
            $rootScope.password = return_data.substr(indx2);
            var q = " SELECT * FROM tbl_users WHERE username='"+  data.username +"'  " ;
            $cordovaSQLite.execute(db, q).then(function(res) {
                if (res.rows.length == 0) {
                    var query1= " INSERT into tbl_users (userid,username,password) values (" + $rootScope.user_id +",'" +data.username + "','" + $rootScope.password + "')";


                    $cordovaSQLite.execute(db, query1).then(function(res) {
                        console.log("Values inserted in tbl_users Table ");

                    }, function (err) {
                        console.error(JSON.stringify(err));
                    });
                }
            }, function (err) {
                console.error(JSON.stringify(err));
            });

            if(authenticated.toString().indexOf('Success')>=0)
            {

                $scope.SelectUserAllocation($rootScope.user_id);
                $scope.SelectKycByUserId($rootScope.user_id);

                $state.go('tab.dash', {}, {reload: true});
                $scope.setCurrentUsername(data.username);
            }

           else
            {

               var alertPopup = $ionicPopup.alert({
                   title: 'Login failed!',
                    template: 'Please check your credentials!'
               });

            }

        }, function(err) {
           $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
                title: 'Login failed!',
                template: 'Please check your credentials!'
            });
        });

    }
        else{

            var qry = "SELECT password, userid from  tbl_users where username='" +$rootScope.user_name+ "'";
            $cordovaSQLite.execute(db, qry).then(function (res) {
                if (res.rows.length > 0) {

                    for (var i = 0; i < res.rows.length; i++) {
                        $ionicLoading.hide();
                        var obj = {};
                        obj = {
                            password: res.rows.item(i).password,
                            userid : res.rows.item(i).userid
                        }
                        results.push(obj);
                    }
                    $scope.Userpassword= results;
                    $scope.password=$scope.Userpassword[0].password;
                    $scope.userid =$scope.Userpassword[0].userid;
                    if( $scope.password ==  data.password){
                        $rootScope.user_id=$scope.userid;
                        $ionicLoading.hide();
                        $state.go('tab.dash', {}, {reload: true});
                        $scope.setCurrentUsername(data.username);
                    }

                }
                else{
                    $ionicLoading.hide();
                    var alertPopup = $ionicPopup.alert({
                        title: 'Mismatch Credentials !',
                        template: 'Your UserName or Password is Incorrect!'
                    });
                }
            })
        }
    };

        $scope.setCurrentUsername = function(name) {
            $scope.username = name;
        };


        $scope.SelectKycByUserId = function (userid) {
           var approved_count=0;
            var pending_approval_count= 0;
            var rejected_count = 0;

            kycServiceGetData.SelectKycByUserId($http, $q, userid).then(function (data) {
                $scope.KycByUserId=data;
                 for(i=0;i<data.length;i++)
                 {
                     var query = "UPDATE tbl_kyc SET approval_status = "+ data[i].approval_status + ",reject_remarks = " + "'"+ data[i].reject_remarks + "'"+ ",reject_reason=" + "'"+ data[i].reject_reason + "'"+ " where db_key ="+ data[i].kyc_id+"";

                     $cordovaSQLite.execute(db, query);

                     if (data[i].approval_status ==0){
                         pending_approval_count = pending_approval_count+1;

                     }
                     if (data[i].approval_status ==1){
                         approved_count = approved_count+1;

                     }

                     if (data[i].approval_status ==2){
                         rejected_count = rejected_count+1;


                     }
                 }
                $rootScope.pending_approval_count=pending_approval_count;
                $rootScope.approved_count=approved_count;
                $rootScope.rejected_count=rejected_count;
            })

        }

        $scope.SelectUserAllocation = function (userid) {
            var qry=  "DELETE  from user_allocation ";
            $cordovaSQLite.execute(db,qry);
            kycServiceGetData.SelectUserAllocation($http, $q, userid).then(function (data) {

                            for(i=0;i<data.length;i++)
                            {
                             var userAreaID = data[i].userAreaID;
                             var areaid =   data[i].areaid;
                             var state = data[i].state;
                             var district =data[i].district;
                             var city = data[i].city;
                             var area = data[i].area;
                             var pincode = data[i].pincode;

                             var query=  "INSERT INTO user_allocation (userAreaID,areaid,state,district,city,area,pincode)  VALUES (?,?,?,?,?,?,?) ";
                                $cordovaSQLite.execute(db, query,[userAreaID,areaid,state,district,city,area,pincode]).then(function(res) {

                                      }, function (err) {
                                    console.error(JSON.stringify(err));
                                });


                            }

            })

        }

})
