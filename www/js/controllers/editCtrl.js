/**
 * Created by atul on 3/23/2016.
 */
angular.module('kyc')
    .controller('EditCtrl', function($state,$scope,$rootScope,$cordovaSQLite,kycServiceSaveData,$http,$q ,$ionicLoading,$ionicPopup,SavephotoService,$stateParams) {

        var rowId = $stateParams.rowId;
                $scope.editorEnabled = false;

        $scope.enableEditor = function() {
            if($scope.kyc_lists[0].uploaded =='No' ) {
                $scope.editorEnabled = true;
                $scope.editableTitle = $scope.title;
            }
            else{
                var alertPopup = $ionicPopup.alert({
                        title: 'Message',
                        template: 'Uploaded Form are not Allowed for EDIT !'
            })
            }
        };

        $scope.disableEditor = function() {
            $scope.editorEnabled = false;
        };

        $scope.save = function(kycdata) {
            $scope.title = $scope.editableTitle;
            $scope.disableEditor();
            $scope.saveData(kycdata);

        };

        $scope.saveData = function (kycdata) {
            console.log('inside saveData');
       console.log(JSON.stringify(kycdata));
                $scope.kyc = angular.copy(kycdata);

 var query="UPDATE tbl_kyc SET form_no = " +"'"+ $scope.kyc.form_no + "'"+ ", owner_name = " +"'"+ $scope.kyc.owner_name + "'"+ ", bussiness_name = " +"'"+ $scope.kyc.bussiness_name + "'"+ ", building_name = " +"'"+ $scope.kyc.building_name + "'"+ ", landmark = " +"'"+ $scope.kyc.landmark +  "'"+ " , state = " +"'"+ $scope.kyc.state + "'"+ ", district = " +"'"+ $scope.kyc.district + "'"+ ", city = " +"'"+ $scope.kyc.city + "'"+ ", area = " +"'"+ $scope.kyc.area + "'"+ " , pincode = " + $scope.kyc.pincode + ", mobile_1 = " + $scope.kyc.mobile_1 + ", mobile_2 = " + $scope.kyc.mobile_2 + ", landline = " + $scope.kyc.landline + ", email = " +"'"+ $scope.kyc.email + "'"+ ",  website = " +"'"+ $scope.kyc.website + "'"+ ", shop_size = " + $scope.kyc.shop_size + " where rowid =" + rowId ;

  $cordovaSQLite.execute(db, query).then(function(res) {
          console.log('Succeessfully run query')
  },
      function (err) {
                      console.error(JSON.stringify(err));
                   });
  };


        $scope.loadLocalData= function (rowId) {
            var brndlist = "";
            var results = [];
            var brand_rslt = [];
            var total_upload = 0;

            var query = "SELECT rowid, B.*, ";
            query += "(SELECT group_concat(brand_name,',') ";
            query += "FROM tbl_kyc_brand A inner join mstr_brands C on C.brndid = A.brand_id ";
            query += "where mapping = 1 AND A.kyc_id = B.rowid GROUP BY A.kyc_id) as brnds ";
            query += "FROM tbl_kyc B where rowid=" + rowId;
            console.log(query);
            var res = $cordovaSQLite.execute(db, query).then(function (res) {


                    if (res.rows.length > 0) {
                        for (var i = 0; i < res.rows.length; i++) {

                            var obj = {};
                            obj = {
                                uploaded: res.rows.item(i).uploaded,
                                row_id: res.rows.item(i).rowid,
                                form_no: res.rows.item(i).form_no,
                                owner_name: res.rows.item(i).owner_name,
                                bussiness_name: res.rows.item(i).bussiness_name,
                                building_name: res.rows.item(i).building_name,
                                landmark: res.rows.item(i).landmark,
                                state: res.rows.item(i).state,
                                district: res.rows.item(i).district,
                                city: res.rows.item(i).city,
                                area: res.rows.item(i).area,
                                pincode: res.rows.item(i).pincode,
                                mobile_1: res.rows.item(i).mobile_1,
                                mobile_2: res.rows.item(i).mobile_2,
                                landline: res.rows.item(i).landline,
                                email: res.rows.item(i).email,
                                website: res.rows.item(i).website,
                                shop_size: res.rows.item(i).shop_size,
                                dob: res.rows.item(i).dob,
                                brands: res.rows.item(i).brnds,
                                industry: res.rows.item(i).industry,
                                approval_status: res.rows.item(i).approval_status,
                                reject_remarks: res.rows.item(i).reject_remarks,
                                reject_reason: res.rows.item(i).reject_reason,
                                smartphone: res.rows.item(i).smartphone,
                                competition: res.rows.item(i).competition,
                                tin: res.rows.item(i).tin,
                                existing_ref_no: res.rows.item(i).existing_ref_no,
                                fe_code: res.rows.item(i).fe_code,
                                business_type: res.rows.item(i).business_type,
                                relaxo_brand_present: res.rows.item(i).relaxo_brand_present,
                                competitive_brand_present: res.rows.item(i).competitive_brand_present,
                                competition_instore_present: res.rows.item(i).competition_instore_present,
                                salesman_count: res.rows.item(i).salesman_count,
                                intSellRelaxoProd : res.rows.item(i).intSellRelaxoProd

                            }
                            if(obj.approval_status == 0)
                            {
                                obj.approval_status ='Pending';
                            }
                            if(obj.approval_status == 1)
                            {
                                obj.approval_status ='Approved';
                            }
                            if(obj.approval_status == 2)
                            {
                                obj.approval_status ='Rejected';
                            }

                            if (obj.uploaded == 0) {
                                obj.uploaded = 'No';
                            }
                            else {
                                obj.uploaded = 'Yes';
                            }
                            if (obj.relaxo_brand_present == 0) {
                                obj.relaxo_brand_present = 'No';
                            }
                            else {
                                obj.relaxo_brand_present = 'Yes';
                            }
                            if (obj.competitive_brand_present == 0) {
                                obj.competitive_brand_present = 'No';
                            }
                            else {
                                obj.competitive_brand_present = 'Yes';
                            }
                            if (obj.competition_instore_present == 0) {
                                obj.competition_instore_present = 'No';
                            }
                            else {
                                obj.competition_instore_present = 'Yes';
                            }

                            results.push(obj);

                        }
                        $scope.kyc_lists = results;
                        $scope.$root.kyc_lists_count = results.length;
                        $scope.total_upload = total_upload + results.length;
                    }
                    $scope.qty = results.length;
                    $scope.$watch(function (scope) {
                            return scope.qty
                        },
                        function (newValue, oldValue) {
                            $scope.$root.kyc_lists_count = newValue;
                        }
                    );

                },
                function (err) {
                    console.error(JSON.stringify(err));
                });


        }
        init();
        function init(){

            $scope.loadLocalData(rowId);
        }


    })